package main

import (
	"net/http"
	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"github.com/gin-gonic/gin"
	"sns-server/sns-server/models"
)

func main() {
	router := gin.Default()
	router.POST("/customer/register", models.Register)
	router.POST("/login", models.Login)
	//添加群组中间件
	authorized := router.Group("/user", MyMiddelware())
	authorized.POST("/info", func(c *gin.Context) {
		c.String(http.StatusOK, "info")
	})
	router.Run(":8080")
}
func MyMiddelware() gin.HandlerFunc {
	return func(c *gin.Context) {
		token, err := request.ParseFromRequest(c.Request, request.AuthorizationHeaderExtractor,
			func(token *jwt.Token) (interface{}, error) {
				return []byte("mobile"), nil
			})
		if err == nil {
			if token.Valid {
				c.Next()
			} else {
				c.String(http.StatusUnauthorized, "Token is not valid")
			}
		} else {
			c.String(http.StatusUnauthorized, "Unauthorized access to this resource")
		}
	}
}


