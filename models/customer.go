package models

import (
	"time"
	"github.com/dgrijalva/jwt-go"
)
type Customer struct {
	Id        int       `xorm:"pk autoincr"`
	Username  string    `xorm:"index unique notnull"`
	Password  string    `xorm:"notnull"`
	CreatedAt time.Time `xorm:"created"`
}
func (c *Customer) Register() string {
	Db.Insert(c)
	has, _ := Db.Get(c)
	if has {
		return JsonResponse(0, "注册成功")
	} else {
		return JsonResponse(1, "注册失败")
	}
}
func (c *Customer) Login() string {
	has, _ := Db.Get(c)
	if has {
		return JsonResponse(0, setToken())
	} else {
		return JsonResponse(1, "登录失败")
	}
}
func setToken() string {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := make(jwt.MapClaims)
	claims["exp"] = time.Now().Add(time.Hour * time.Duration(1)).Unix()
	claims["iat"] = time.Now().Unix()
	token.Claims = claims
	tokenString, err := token.SignedString([]byte("mobile"))
	if err != nil {
		return ""
	}
	return tokenString
}
