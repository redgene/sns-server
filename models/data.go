package models

import (
	"encoding/json"
	"fmt"
	"net/http"
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
	"github.com/gin-gonic/gin"
)
var Db *xorm.Engine
func init() {
	var err error
	//打开数据库
	//DSN数据源字符串：用户名:密码@协议(地址:端口)/数据库?参数=参数值
	Db, err = xorm.NewEngine("mysql", "root:123@tcp(192.168.33.61:3306)/test?charset=utf8")
	if err != nil {
		fmt.Println(err)
	}
	Db.ShowSQL(true)
	err = Db.Sync2(new(Customer))
}
func JsonResponse(code int, data interface{}) string {
	response := make(map[string]interface{})
	response["code"] = code
	response["data"] = data
	js, err := json.Marshal(response)
	if err != nil {
		return err.Error()
	}
	return string(js)
}
func Register(c *gin.Context) {
	username := c.PostForm("username")
	password := c.PostForm("password")
	user := Customer{
		Username: username,
		Password: password,
	}
	c.String(http.StatusOK, user.Register())
}
func Login(c *gin.Context) {
	username := c.PostForm("username")
	password := c.PostForm("password")
	user := Customer{
		Username: username,
		Password: password,
	}
	c.String(http.StatusOK, user.Login())
}

